package fr.remipicard.servlet;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HelloServlet
 */
public class EnvServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public EnvServlet() {}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		response.getWriter().append(System.getenv("JAVA_HOME"));
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Map<String, String> map = new HashMap<>();
		map.put("JAVA_HOME", request.getParameter("val"));
		try {
			setEnv(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		doGet(request, response);
	}

	/**
	* @see https://stackoverflow.com/questions/318239/how-do-i-set-environment-variables-from-java
	*/
	protected void setEnv(Map<String, String> newenv) throws Exception {
		Class<?> processEnvironmentClass = Class.forName("java.lang.ProcessEnvironment");
		Field theEnvironmentField = processEnvironmentClass.getDeclaredField("theEnvironment");
		theEnvironmentField.setAccessible(true);
		Map<String, String> env = (Map<String, String>) theEnvironmentField.get(null);
		env.putAll(newenv);
		Field theCaseInsensitiveEnvironmentField = processEnvironmentClass
				.getDeclaredField("theCaseInsensitiveEnvironment");
		theCaseInsensitiveEnvironmentField.setAccessible(true);
		Map<String, String> cienv = (Map<String, String>) theCaseInsensitiveEnvironmentField.get(null);
		cienv.putAll(newenv);
	}

}
